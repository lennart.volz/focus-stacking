import sys
import numpy as np
import cv2
import matplotlib.pyplot as plt
import ROOT as rt
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from scipy.signal import savgol_filter


################################################################## 
# Specific parameters that need fine tuning 
kernel_size = 5        # Size of the Laplacian kernel
blur_size   = 5        # size of the Gaussian kernel
savgol_region = 11     # Region used for savgol filter adaption
savgol_pol_order = 2   # Polynomial order for savgol smoothening 
#################################################################

def cv2_clipped_zoom(img, zoom_factor):
    """
    Center zoom in/out of the given image and returning an enlarged/shrinked view of 
    the image without changing dimensions
    Args:
    img : Image array
    zoom_factor : amount of zoom as a ratio (0 to Inf)
    """
    height, width = img.shape[:2] # It's also the final desired shape
    new_height, new_width = int(height * zoom_factor), int(width * zoom_factor)
    
    ### Crop only the part that will remain in the result (more efficient)
    # Centered bbox of the final desired size in resized (larger/smaller) image coordinates
    y1, x1 = max(0, new_height - height) // 2, max(0, new_width - width) // 2
    y2, x2 = y1 + height, x1 + width
    bbox = np.array([y1,x1,y2,x2])
    # Map back to original image coordinates
    bbox = (bbox / zoom_factor).astype(int)
    y1, x1, y2, x2 = bbox
    cropped_img = img[y1:y2, x1:x2]
    
    # Handle padding when downscaling
    resize_height, resize_width = min(new_height, height), min(new_width, width)
    pad_height1, pad_width1 = (height - resize_height) // 2, (width - resize_width) //2
    pad_height2, pad_width2 = (height - resize_height) - pad_height1, (width - resize_width) - pad_width1
    pad_spec = [(pad_height1, pad_height2), (pad_width1, pad_width2)] + [(0,0)] * (img.ndim - 2)
    
    result = cv2.resize(cropped_img, (resize_width, resize_height))
    result = np.pad(result, pad_spec, mode='constant')
    assert result.shape[0] == height and result.shape[1] == width
    return result


def FocusStackingRadiograph(name):
    f = rt.TFile(name,'READ')
    hist3D = f.Get('3DRadiograph')

    # Get underlying array from root histogram and convert it to a numpy array    
    Nx = hist3D.GetNbinsX()
    Ny = hist3D.GetNbinsY()
    Nz = hist3D.GetNbinsZ()
    arr = hist3D.GetArray()
    binvals = np.ndarray( ((Nx+2)*(Ny+2)*(Nz+2),), dtype=np.float64, buffer=arr)
    binvals = np.transpose(binvals.reshape((Nz+2,Ny+2,Nx+2),order='C'),(2,1,0))
    # Strip off underflow and overflow bins
    image3D = binvals[1:-1,1:-1,1:-1]



    ##Create the DDB stack (2 and -2 are due to the geometry of the simulation data, please change for your setup!)
    image_stack = []
    for i in range(2,image3D.shape[0]-2):
        image_stack.append(image3D[i,:,:])

   
    #Include enlarging of image for cone-beam geometries: to be tested
    #Source distance to first image in plane
    #source = 4000 #mm

    # Obtain laplacian values
    laps = []
    for i,image in enumerate(image_stack):
        fact = 1 # Parallel beam geometry, no zoom factor used
        #fact = 1+i/source # factor for image enlargement with distance to front tracker 
        res = cv2_clipped_zoom(image, fact) # Enlarging of image with distance (for cone beam geometries)
        
        #Gaussian blur 
        blurred  = cv2.GaussianBlur(image, (blur_size,blur_size), 0)

        #Laplacian convolution
        laplace  = cv2.Laplacian(blurred, cv2.CV_64F, ksize=kernel_size)
        laps.append(laplace)

    #convert list of laps to np array
    laps = np.asarray(laps)
    abs_laps = np.absolute(laps)

    #Prepare output image and depth estimate map
    output = np.zeros(image_stack[0].shape, dtype=np.float64)
    output_depth = np.zeros(image_stack[0].shape, dtype=np.float64)
    for i in range(0,output.shape[0]):
        for j in range(0,output.shape[1]):        
            #Smoothen laplacian values to avoid noise spikes
            abs_laps_hat = savgol_filter(abs_laps[:,i,j], savgol_region, savgol_pol_order) # window size 11, pol order 2

            #Maximum value of absolute laplacian is the estimated depth of focus for this pixel
            max_lap = abs_laps_hat[:].max()

            #Save the first instance where the laplacian reached its max 
            max_id = np.where(abs_laps_hat[:]==max_lap)[0][0]

            # Output image pixel is the pixel value at the maximum laplacian depth from image stack
            output[i,j] = image_stack[max_id][i,j]
            output_depth[i,j] = max_id ## in index position, which for the stack made by phase.C->ReconstructRadiograph is in units of 1mm Adapt to your setup 

    #Plot and save the output image
    fig = plt.figure(figsize=(10,10))
    ax = plt.gca()
    ax.set_axis_off()
    im = plt.imshow(output,cmap='Greys_r',origin='lower',vmin=190, vmax=210)
    ax.set_xlim([100,300])
    ax.set_ylim([100,300])  
    divider = make_axes_locatable(ax) 
    cax = divider.append_axes('right', pad = 0.05, size = '5%')
    plt.colorbar(im, label = "Water equivalent thickness [mm]",cax=cax)
    plt.savefig('FocusStackingImage.png', format='png', dpi=600,  bbox_inches='tight', transparent=False)
    plt.show()
 
    #Plot and save the output depth
    fig = plt.figure(figsize=(10,10))
    ax = plt.gca()
    ax.set_axis_off()
    im = plt.imshow(output_depth,cmap='hot',origin='lower',vmin=0., vmax=200)
    ax.set_xlim([100,300])
    ax.set_ylim([100,300])  
    divider = make_axes_locatable(ax) 
    cax = divider.append_axes('right', pad = 0.05, size = '5%')
    plt.colorbar(im, label = "Radiological depth [mm]",cax=cax)

    plt.savefig('FocusStackingPixelDepth.png', format='png', dpi=600,  bbox_inches='tight', transparent=False)
    plt.show()


def main():
    FocusStackingRadiograph(sys.argv[1])
    
if __name__=='__main__':
    main()
