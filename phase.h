//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jul 25 09:12:22 2022 by ROOT version 6.20/00
// from TTree phase/PS
// found on file: StaggeredCubes_230_0.0_1_1.root
//////////////////////////////////////////////////////////

#ifndef phase_h
#define phase_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "string"

class phase {
public :
   TFile          *f; 	
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         x0;
   Float_t         y0;
   Float_t         z0;
   Float_t         y0pb;
   Float_t         z0pb;
   Float_t         px0;
   Float_t         py0;
   Float_t         pz0;
   Float_t         Einit;
   Float_t         x1;
   Float_t         y1;
   Float_t         z1;
   Float_t         px1;
   Float_t         py1;
   Float_t         pz1;
   Float_t         Estop;
   Double_t        WET;
   string          *proc_name;
   string          *part_name;
   Int_t           EventID;
   Int_t           idPBY;
   Int_t           idPBZ;

   // List of branches
   TBranch        *b_x0;   //!
   TBranch        *b_y0;   //!
   TBranch        *b_z0;   //!
   TBranch        *b_y0pb;   //!
   TBranch        *b_z0pb;   //!
   TBranch        *b_px0;   //!
   TBranch        *b_py0;   //!
   TBranch        *b_pz0;   //!
   TBranch        *b_Einit;   //!
   TBranch        *b_x1;   //!
   TBranch        *b_y1;   //!
   TBranch        *b_z1;   //!
   TBranch        *b_px1;   //!
   TBranch        *b_py1;   //!
   TBranch        *b_pz1;   //!
   TBranch        *b_Estop;   //!
   TBranch        *b_WET;   //!
   TBranch        *b_proc_name;   //!
   TBranch        *b_part_name;   //!
   TBranch        *b_EventID;   //!
   TBranch        *b_idPBY;   //!
   TBranch        *b_idPBZ;   //!

   phase(TTree *tree=0,TString = "");
   virtual ~phase();
   virtual void     ReconstructRadiograph(); 
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef phase_cxx
phase::phase(TTree *tree, TString name) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
     // f = (TFile*)gROOT->GetListOfFiles()->FindObject("StaggeredCubes_230_0.0_1_1.root","UPDATE");
     // if (!f || !f->IsOpen()) {
         f = new TFile(name.Data(),"UPDATE");
      //}
      f->GetObject("phase",tree);

   }
   Init(tree);
}

phase::~phase()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t phase::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t phase::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void phase::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   proc_name = 0;
   part_name = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("x0", &x0, &b_x0);
   fChain->SetBranchAddress("y0", &y0, &b_y0);
   fChain->SetBranchAddress("z0", &z0, &b_z0);
   fChain->SetBranchAddress("y0pb", &y0pb, &b_y0pb);
   fChain->SetBranchAddress("z0pb", &z0pb, &b_z0pb);
   fChain->SetBranchAddress("px0", &px0, &b_px0);
   fChain->SetBranchAddress("py0", &py0, &b_py0);
   fChain->SetBranchAddress("pz0", &pz0, &b_pz0);
   fChain->SetBranchAddress("Einit", &Einit, &b_Einit);
   fChain->SetBranchAddress("x1", &x1, &b_x1);
   fChain->SetBranchAddress("y1", &y1, &b_y1);
   fChain->SetBranchAddress("z1", &z1, &b_z1);
   fChain->SetBranchAddress("px1", &px1, &b_px1);
   fChain->SetBranchAddress("py1", &py1, &b_py1);
   fChain->SetBranchAddress("pz1", &pz1, &b_pz1);
   fChain->SetBranchAddress("Estop", &Estop, &b_Estop);
   fChain->SetBranchAddress("WET", &WET, &b_WET);
   fChain->SetBranchAddress("proc_name", &proc_name, &b_proc_name);
   fChain->SetBranchAddress("part_name", &part_name, &b_part_name);
   fChain->SetBranchAddress("EventID", &EventID, &b_EventID);
   fChain->SetBranchAddress("idPBY", &idPBY, &b_idPBY);
   fChain->SetBranchAddress("idPBZ", &idPBZ, &b_idPBZ);
   Notify();
}

Bool_t phase::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void phase::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t phase::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef phase_cxx
