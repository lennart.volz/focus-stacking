# focus-stacking
![Overview](FocusStackingOverview-Paper.png){width=50%}


# Whats it for
This project enables you to apply focus stacking to single-event particle radiography (pRad) data. In single-event particle imaging, each particle's position and direction before and after the patient is recorded from which the particles most likely trajectory through the patient can calculated. This 3D particle path enables to bin the particle histories at different depths inside the patient into projection images (DDB), as proposed by Rit et al. (2013) Med. Phys. Due to the particle scattering, there is a unique 'depth of focus' for each feature inside the patient. Computing a series of DDB reconstructions at different depth through the object (DDB stack) presents therefore a good basis for focus stacking, which is a technique common to optical photography or microscopy. In focus stacking, the image out of the stack with the highest spatial resolution for each feature is determined. All DDB images in the DDB stack are then collapsed into a single image, by stiching together the pixels that yielded the highest spatial resolution. This project achieves this by utilizing the Laplacian operator. 


# General structure
The code is differentiated in a reconstruction part (phase.C, phase.h) which builds on ROOT, which provides the DDB stack, and a focus stacking part (FocusStackingpRad.py). The focus stacking utilizes convolution functions available through the cv2 library. The requirements for the python code are listed in a requirements.txt. You can install these by running pip install -r requirements.txt. The reconstruction code only depends on ROOT, and was built for ROOT6. 

An example root file containing the single-event pRad data from a Geant4 Monte Carlo simulation is provided.

# Getting the DDB stack
In order to obtain the DDB stack run root, and then in the root prompt enter: 

- .L phase.C+
- phase t(0,"StaggeredCubes_200_0.0_1_1.root"); 
- t.ReconstructRadiograph();

This will yield the DDB stack (image3D), as well as a front-tracker binned pRad (image2DFTB) and a pRad reconstructed with the maximum likelihood formalism by Collins-Fekete et al. (2016) PMB. 

# Running the focus stacking algorithm
You can then run the focus stacking as: 

python3 FocusStackingpRad.py StaggeredCubes_200_0.0_1_1.root

# Dependencies
The image reconstruction code (phase.C + phase.h) is based on ROOT 6.20/00

The focus stacking code is based on python.  For the specific libraries required, a requirements.txt is provided, where libraries are listed. 

